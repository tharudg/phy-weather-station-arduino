void serialPrint(String str){
  Serial.println( "Temparature: " + getValue(str, ',', 0)  + "\t"
                + "pressure   : " + getValue(str, ',', 1)     + "\t"
                + "Humidity   : " + getValue(str, ',', 2)     + "\t"
                + "altitude   : " + getValue(str, ',', 3)     + "\t"
                + "WindSpeed  : " + getValue(str, ',', 4)    + "\t"
                + "Direction  : " + getValue(str, ',', 5));
  }


String getValue(String data, char separator, int index){
    int found = 0;
    int strIndex[] = { 0, -1 };
    int maxIndex = data.length() - 1;

    for (int i = 0; i <= maxIndex && found <= index; i++) {
        if (data.charAt(i) == separator || i == maxIndex) {
            found++;
            strIndex[0] = strIndex[1] + 1;
            strIndex[1] = (i == maxIndex) ? i+1 : i;
        }
    }
    return found > index ? data.substring(strIndex[0], strIndex[1]) : "";
}
