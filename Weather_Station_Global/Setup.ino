void setup()
{
  Serial.begin(9600);
  Serial1.begin(115200);
  
  tipCount = 0;
  totalRainfall = 0;
  rotations = 0;
  isSampleRequired = false;
  timerCount = 0;
  
  pinMode(RG11_Pin, INPUT);
  pinMode(WindSensor_Pin, INPUT);
  attachInterrupt(digitalPinToInterrupt(RG11_Pin), isr_rg, FALLING);
  attachInterrupt(digitalPinToInterrupt(WindSensor_Pin), isr_rotation, FALLING);
  Timer1.initialize(500000);
  Timer1.attachInterrupt(isr_timer);
  sei();

} 
