//Libraries include here

#include "dht.h"
#include <Wire.h>
#include <Adafruit_BMP085.h>
#include <ArduinoJson.h>
#include "TimerOne.h"
#include <math.h>
 
//Define pins here
#define dht_apin 7 // Analog Pin sensor is connected to
#define windDirectionPin A1
#define WindSensor_Pin (2)
#define RG11_Pin 3 
#define Bucket_Size 0.01 // bucket size to trigger tip count

//Create object here
dht DHT;
Adafruit_BMP085 bmp;

//Define global variables here
int vaneValue;      // raw analog value from wind vane  
int calDirection;   // converted value with offset applied 
int lastValue;      // last direction value

volatile unsigned long tipCount;
volatile unsigned long contactTime;

volatile bool isSampleRequired;
volatile unsigned int timerCount;
volatile unsigned long rotations;
volatile unsigned long contactBounceTime;

volatile float windSpeed;
volatile float totalRainfall; // total amount of rainfall detected

#define offset 0
float rainfall = 0;
float rainfall2 = 0;


//Function prototypes

void dhtInit();
float getTemperature();
float getHumidity();

//void barometerInit();
float getPressure();
float getAltitude();

void upload(String str);                                    //function to upload data to realtime nats server
void serialPrint(String str);                               //prints sensor data to serial monitor
String getValue(String data, char separator, int index);    //function to separate string using separator

void isr_rotation ();
float getWindSpeed();
int getWindDirection();
void isr_timer();
void isr_rg();

float getRainfall();
void initRainfall();


void isr_timer();





/**
  JSON globals
**/

DynamicJsonDocument doc(1024);
