String str="";
void loop(){


 
  
  if(isSampleRequired) {
    
    dhtInit();
    barometerInit();
    delay(1000);
    str =    String(getTemperature())           + ","
                  + String(getPressure())       + ","
                  + String(getHumidity())       + ","
                  + String(getAltitude())       + ","
                  + String(windSpeed)           + ","
                  + String(totalRainfall)       + ","
                  + String(getWindDirection()
                  );
    upload(str);
    Serial.println(str);
    isSampleRequired = false;
 
  }
}


void isr_timer() {
  timerCount++;
  if(timerCount == 5) {
    windSpeed = rotations * 0.9*1.60934;
    rotations = 0;
    isSampleRequired = true;
    timerCount = 0;
  }
}

void isr_rotation() {
  if((millis() - contactBounceTime) > 15 ) { 
    rotations++;
    contactBounceTime = millis();
  }
}

void isr_rg() {
  if((millis() - contactTime) > 15 ) { 
    tipCount++;
    totalRainfall = tipCount * Bucket_Size;
    contactTime = millis();
  }
}
